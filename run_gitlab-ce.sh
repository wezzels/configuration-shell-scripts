#!/bin/sh
#Basic instructions for making a home gitlab server on port 8090.  

# /nfs is a remove disk share.
mkdir /nfs/gitlab
mkdir /nfs/gitlab/config
mkdir /nfs/gitlab/logs
mkdir /nfs/gitlab/data
mkdir /nfs/gitlab/backup

sudo docker run --detach \
  --hostname brix16.home.wezzel.com \
  --publish 9443:443 --publish 8090:80 --publish 2222:22 \
  --name gitlab \
  --restart always \
  --volume /nfs/gitlab/config:/etc/gitlab \
  --volume /nfs/gitlab/logs:/var/log/gitlab \
  --volume /nfs/gitlab/data:/var/opt/gitlab \
  gitlab/gitlab-ce:latest

