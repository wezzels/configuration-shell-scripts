sudo docker run -d --restart=unless-stopped \
    -v /data/rancher:/var/lib/rancher \
    -v /data/rancher/server/mysql:/var/lib/mysql \
    -p 8085:80 -p 8133:443 rancher/rancher:latest
