mkdir -p /data/etherpad/mysqldata
docker network create ep_network
docker run -d --network ep_network \
     -v /data/etherpad/mysql:/var/lib/mysql \
     -e MYSQL_ROOT_PASSWORD=password123 \
     --name ep_mysql mysql
docker run -d \
    --network ep_network \
    -e ETHERPAD_DB_HOST=ep_mysql \
    -e ETHERPAD_DB_PASSWORD=password123 \
    -e ETHERPAD_ADMIN_PASSWORD=password123 \
    -p 9001:9001 \
    tvelocity/etherpad-lite
