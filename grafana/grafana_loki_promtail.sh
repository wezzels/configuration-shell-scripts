


docker run -d \
-p 3000:3000 \
--name=grafana \
-e "GF_INSTALL_PLUGINS=grafana-clock-panel,grafana-simple-json-datasource" \
grafana/grafana
wget https://raw.githubusercontent.com/grafana/loki/v1.3.0/cmd/loki/loki-local-config.yaml -o loki-config.yaml
docker run -v $(pwd):/mnt/config -p 3100:3100 grafana/loki:v1.3.0 -config.file=/mnt/config/loki-config.yaml
wget https://raw.githubusercontent.com/grafana/loki/v1.3.0/cmd/promtail/promtail-docker-config.yaml -o promtail-config.yaml
docker run -v $(pwd):/mnt/config -v /var/log:/var/log grafana/promtail:latest -config.file=/mnt/config/promtail-config.yaml
