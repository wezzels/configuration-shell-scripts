#!/usr/bin/bash
population_s="2096640"
population_c="327200000"
population_w="7770173166"

#US
echo "US Stats"
curl -s https://corona.help/country/us/ | grep -m3 text-bold-700 | cut -d">" -f2 | cut -d"<" -f1 > US
declare -a us
readarray us < ./US
printf "Total: %s Deaths: %s Recovered: %s  " "$( echo ${us[0]}|tr -d '\n' )" "$( echo ${us[1]}|tr -d '\n' )" "$( echo ${us[2]}|tr -d '\n' )"
#State
echo
echo "New Mexico"
curl -s https://corona.help/country/us/state/new-mexico | grep -m3 text-bold-700 | cut -d">" -f2 | cut -d"<" -f1 > state
declare -a state
readarray state < ./state
printf "Total: %s Deaths: %s Recovered: %s  " "$( echo ${state[0]}|tr -d '\n' )" "$( echo ${state[1]}|tr -d '\n' )" "$( echo ${state[2]}|tr -d '\n' )"
#World
echo
echo "World"
curl -s https://corona.help/ | grep -m3 text-bold-700 | cut -d">" -f2 | cut -d"<" -f1 > world
declare -a world
readarray world < ./world
echo "Total: $( echo ${world[0]}|tr -d '\n' ) Deaths: $( echo ${world[1]}|tr -d '\n' ) Recovered: $( echo ${world[2]}|tr -d '\n' )"
#Stats
echo "What are the odds?"
world_stat=`echo ${world[0]} | tr -d ',' `
country_stat=`echo ${us[0]} | tr -d ','` 
state_stat=`echo ${state[0]} | tr -d ',' `
world_stat=$( echo "scale=8; $world_stat/$population_w" | bc)
country_stat=$( echo "scale=8; $country_stat/$population_c" | bc)
state_stat=$( echo "scale=8; $state_stat/$population_s" | bc)
echo "World: $world_stat% US: $country_stat% NM: $state_stat%"
