docker create \
  --name=heimdall \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=America/Denver \
  -p 10080:80 \
  -p 10443:443 \
  -v /data/heimdall/config:/config \
  --restart unless-stopped \
  linuxserver/heimdall
