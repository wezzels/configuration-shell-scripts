#!/bin/sh

ssh pi@10.0.0.101 sudo cat /proc/cpuinfo | grep Model | tr -s ' ' | cut -d':' -f 2
ssh pi@10.0.0.101 df -h -T | grep -v tmpfs
ssh pi@10.0.0.101 sudo vcgencmd measure_temp 
ssh pi@10.0.0.101 sudo vcgencmd measure_volts

ssh pi@10.0.0.102 sudo cat /proc/cpuinfo | grep Model | tr -s ' ' | cut -d':' -f 2
ssh pi@10.0.0.102 df -h -T | grep -v tmpfs
ssh pi@10.0.0.102 sudo vcgencmd measure_temp
ssh pi@10.0.0.102 sudo vcgencmd measure_volts

ssh pi@10.0.0.103 sudo cat /proc/cpuinfo | grep Model | tr -s ' ' | cut -d':' -f 2
ssh pi@10.0.0.103 df -h -T | grep -v tmpfs
ssh pi@10.0.0.103 sudo vcgencmd measure_temp
ssh pi@10.0.0.103 sudo vcgencmd measure_volts

ssh pi@10.0.0.104 sudo cat /proc/cpuinfo | grep Model | tr -s ' ' | cut -d':' -f 2
ssh pi@10.0.0.104 df -h -T | grep -v tmpfs
ssh pi@10.0.0.104 sudo vcgencmd measure_temp
ssh pi@10.0.0.104 sudo vcgencmd measure_volts

#ssh pi@10.0.0.105 sudo cat /proc/cpuinfo | grep Model | tr -s ' ' | cut -d':' -f 2
#ssh pi@10.0.0.105 df -h -T | grep -v tmpfs
#ssh pi@10.0.0.105 sudo vcgencmd measure_temp 
#ssh pi@10.0.0.105 sudo vcgencmd measure_volts

ssh pi@10.0.0.106 sudo cat /proc/cpuinfo | grep Model | tr -s ' ' | cut -d':' -f 2
ssh pi@10.0.0.106 df -h -T | grep -v tmpfs
ssh pi@10.0.0.106 sudo vcgencmd measure_temp
ssh pi@10.0.0.106 sudo vcgencmd measure_volts

ssh pi@10.0.0.107 sudo cat /proc/cpuinfo | grep Model | tr -s ' ' | cut -d':' -f 2
ssh pi@10.0.0.107 df -h -T | grep -v tmpfs
ssh pi@10.0.0.107 sudo vcgencmd measure_temp
ssh pi@10.0.0.107 sudo vcgencmd measure_volts

ssh pi@10.0.0.108 sudo cat /proc/cpuinfo | grep Model | tr -s ' ' | cut -d':' -f 2
ssh pi@10.0.0.108 df -h -T | grep -v tmpfs
ssh pi@10.0.0.108 sudo vcgencmd measure_temp
ssh pi@10.0.0.108 sudo vcgencmd measure_volts
